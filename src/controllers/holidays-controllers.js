import { NotFound } from '../helpers/errors';
import { Op } from 'sequelize';
import db from '../models';

const Holiday = db.Holiday;

export const index = async (req, res) => {
  const { data, data_inicio, data_fim } = req.query;
  
  let where = {};
  if (data) where.date = data;
  
  if (data_inicio && data_fim) {
    where = { date: { [Op.between]: [data_inicio, data_fim] } };
  }
  
  const holidays = await Holiday.findAll({
    where,
    attributes: ['description', 'date'],
  });
  
  return res.status(200).json(holidays);
};

export const show = async (req, res) => {
  const { id_feriado } = req.params;
  
  const holiday = await Holiday.findOne({
    where: { id: id_feriado },
    attributes: ['description', 'date'],
  });
  
  if (!holiday) throw new NotFound('Feriado não encontrado');
  
  return res.status(200).json(holiday);
};

export const store = async (req, res) => {
  const { descricao, data } = req.body;
  
  const newHoliday = await Holiday.create({
    description: descricao,
    date: data,
  });
  
  const holiday = await Holiday.findOne({
    where: { id: newHoliday.id },
    attributes: ['id', 'description', 'date'],
  });
  
  return res.status(201).json(holiday);
};

export const update = async (req, res) => {
  const { id_feriado } = req.params;
  const { descricao, data } = req.body;
  
  const holiday = await Holiday.findOne({ where: { id: id_feriado } });
  if (!holiday) throw new NotFound('Feriado não encontrado');
  
  await Holiday.update(
    {
      description: descricao,
      date: data,
    },
    { where: { id: holiday.id } },
  );
  
  return res.status(204).end();
};

export const destroy = async (req, res) => {
  const { id_feriado } = req.params;
  
  const holiday = await Holiday.findOne({ where: { id: id_feriado } });
  if (!holiday) throw new NotFound('Feriado não encontrado');
  
  await Holiday.destroy({ where: { id: holiday.id } });
  
  return res.status(204).end();
};

export default { index, show, store, update, destroy };
