import { NotFound, UnprocessableEntity, Conflict, ckeckCnpj, } from '../helpers';
import db from '../models';

const Company = db.Company;

export const index = async (req, res) => {
  const { valor_hora , cnpj } = req.query;

  const where = {};
  if (valor_hora) where.hour_value = valor_hora;
  if (cnpj) where.cnpj = cnpj;
  
  const companies = await Company.findAll({
    where,
    attributes: ['name', 'cnpj', 'founding_date', 'hour_value'],
  });
  
  return res.status(200).json(companies);
};

export const show = async (req, res) => {
  const { id_empresa } = req.params;
  
  const company = await Company.findOne({
    where: { id: id_empresa },
    attributes: ['name', 'cnpj', 'founding_date', 'hour_value'],
  });
  
  if (!company) throw new NotFound('Empresa não encontrada');
  
  return res.status(200).json(company);
};

export const store = async (req, res) => {
  const { nome, cnpj, data_fundacao, valor_hora } = req.body;

  const checkUniqueCnpj = await _uniqueCnpj(cnpj);
  if (!checkUniqueCnpj) throw new Conflict('CNPJ já cadastrado');

  const checkValidCnpj = await ckeckCnpj(cnpj);
  if (!checkValidCnpj) throw new UnprocessableEntity('CNPJ inválido');

  const newCompany = await Company.create({
    name: nome,
    cnpj: cnpj,
    founding_date: data_fundacao,
    hour_value: valor_hora,
  });
  
  const company = await Company.findOne({
    where: { id: newCompany.id },
    attributes: ['id', 'name', 'cnpj', 'founding_date', 'hour_value'],
  });
  
  return res.status(201).json(company);
};

export const update = async (req, res) => {
  const { id_empresa } = req.params;
  const { nome, cnpj, data_fundacao, valor_hora } = req.body;

  const company = await Company.findOne({ where: { id: id_empresa } });
  if (!company) throw new NotFound('Empresa não encontrada');

  if (cnpj) {
    const checkUniqueCnpj = await _uniqueCnpj(cnpj);
    if (!checkUniqueCnpj) throw new Conflict('CNPJ já cadastrado');

    const validCnpj = await ckeckCnpj(cnpj);
    if (!validCnpj) throw new UnprocessableEntity('CNPJ inválido');
  }

  await Company.update(
    {
      name: nome,
      cnpj: cnpj,
      founding_date: data_fundacao,
      hour_value: valor_hora,
    },
    { where: { id: company.id } },
  );
  
  return res.status(204).end();
};

export const destroy = async (req, res) => {
  const { id_empresa } = req.params;
  
  const company = await Company.findOne({ where: { id: id_empresa } });
  if (!company) throw new NotFound('Empresa não encontrada');
  
  await Company.destroy({ where: { id: company.id } });
  
  return res.status(204).end();
};

async function _uniqueCnpj(cnpj) {
  const company = await Company.findOne({ where: { cnpj: cnpj } });
  if (!company) 
    return true;
}

export default { index, show, store, update, destroy };
