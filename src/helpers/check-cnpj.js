import fetch from 'node-fetch';

export const ckeckCnpj = async (cnpj) => {
  const cnpjNumber = cnpj.replace(/[^0-9]/g, '');

  if (cnpjNumber.length !== 14) return false;

  const response = await fetch(
    `https://www.receitaws.com.br/v1/cnpj/${cnpjNumber}`,
  );

  const dados = await response.json();

  const { message } = dados;
  if (message === 'CNPJ inválido') return false;
  
  return true
};
