export class NotFound extends Error {
  constructor(message = 'The requested resource could not be found') {
    super(message);
    this.name = 'NotFound';
    this.statusCode = 404;
    this.errorCode = 404;
  }
}

export class UnprocessableEntity extends Error {
  constructor(
    message = 'Validation not processed because some invalid value was sent in the reques',
  ) {
    super(message);
    this.name = 'UnprocessableEntity';
    this.statusCode = 422;
    this.errorCode = 422;
  }
}

export class Conflict extends Error {
  constructor(
    message = 'The request could not be completed, conflicting with the current state of the target resource',
  ) {
    super(message);
    this.name = 'Conflict';
    this.statusCode = 409;
    this.errorCode = 409;
  }
}
