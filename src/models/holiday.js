'use strict';
import { Model } from 'sequelize';
export default (sequelize, DataTypes) => {
  class Holiday extends Model {
    static associate(models) {}
  }
  Holiday.init(
    {
      description: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Por favor digite a descrição do feriado' },
        },
      },
      date: {
        type: DataTypes.DATEONLY,
        validate: {
          isDate: true,
          notEmpty: { msg: 'Por favor digite a data do feriado' },
        },
      },
    },
    {
      sequelize,
      modelName: 'Holiday',
    },
  );
  return Holiday;
};
