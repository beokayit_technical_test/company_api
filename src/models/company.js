'use strict';
import { Model } from 'sequelize';
export default (sequelize, DataTypes) => {
  class Company extends Model {
    static associate(models) {}
  }
  Company.init(
    {
      name: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Por favor digite o nome da empresa' },
          len: {
            args: [4, 50],
            msg: 'O nome deve conter no mínimo 4 e no máximo 50 caracteres',
          },
        },
      },
      cnpj: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Por favor digite o CNPJ' },
        },
      },
      founding_date: {
        type: DataTypes.DATEONLY,
        validate: {
          isDate: true,
          notEmpty: { msg: 'Por favor digite a data de fundação da empresa' },
        },
      },
      hour_value: {
        type: DataTypes.DECIMAL,
        validate: {
          isDecimal: true,
          notEmpty: { msg: 'Por favor digite o valor da hora trabalhada' },
        },
      },
    },
    {
      sequelize,
      modelName: 'Company',
    },
  );
  return Company;
};
