import { Router } from 'express';

import companiesControllers from '../controllers/companies-controllers';

const routes = Router();

routes.get('/empresas', companiesControllers.index);
routes.get('/empresas/:id_empresa', companiesControllers.show);
routes.post('/empresas', companiesControllers.store);
routes.patch('/empresas/:id_empresa', companiesControllers.update);
routes.delete('/empresas/:id_empresa', companiesControllers.destroy);

export default routes;
