import { Router } from 'express';

import holidaysControllers from '../controllers/holidays-controllers';

const routes = Router();

routes.get('/feriados', holidaysControllers.index);
routes.get('/feriados/:id_feriado', holidaysControllers.show);
routes.post('/feriados', holidaysControllers.store);
routes.patch('/feriados/:id_feriado', holidaysControllers.update);
routes.delete('/feriados/:id_feriado', holidaysControllers.destroy);

export default routes;
