import Router from 'express';

import companiesRoutes from './companies-routes';
import holidaysRoutes from './holidays-routes';

const router = new Router();

router.use(companiesRoutes);
router.use(holidaysRoutes);

export default router;
