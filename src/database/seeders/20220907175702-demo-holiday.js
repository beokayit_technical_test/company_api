'use strict';

export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Holidays',
      [
        {
          description: 'Independêcia do Brasil',
          date: '2022-09-07',
        },
        {
          description: 'Nossa Senhora Aparecida',
          date: '2022-10-12',
        },
        {
          description: 'Finados',
          date: '2022-11-02',
        },
      ],
      {},
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Holidays', null, {});
  },
};
