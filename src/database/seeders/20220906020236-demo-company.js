'use strict';

export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Companies',
      [
        {
          name: 'GOOGLE BRASIL INTERNET LTDA',
          cnpj: '06990590000123',
          founding_date: '2004-09-01',
          hour_value: 25.50,
        },
        {
          name: 'NETFLIX ENTRETENIMENTO BRASIL LTDA',
          cnpj: '13590585000199',
          founding_date: '2011-04-27',
          hour_value: 20.00,
        },
        {
          name: 'Mateus e Isabela Buffet Ltda',
          cnpj: '14459351000170',
          founding_date: '2017-09-12',
          hour_value: 25.50,
        },
      ],
      {},
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Companies', null, {});
  },
};
